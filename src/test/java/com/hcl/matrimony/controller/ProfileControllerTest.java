package com.hcl.matrimony.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.matrimony.dto.AddressDto;
import com.hcl.matrimony.dto.ApiResponse;
import com.hcl.matrimony.dto.InterstedProfileDto;
import com.hcl.matrimony.dto.ProfileDto;
import com.hcl.matrimony.entity.Gender;
import com.hcl.matrimony.entity.MaritalStatus;
import com.hcl.matrimony.entity.Religion;
import com.hcl.matrimony.service.ProfileService;

@ExtendWith(SpringExtension.class)
class ProfileControllerTest {
	@Mock
	private ProfileService profileService;
	@InjectMocks
	private ProfileController profileController;

	@Test
	void testUpdateProfile() {
		AddressDto addressDto = AddressDto.builder().lane("asd").city("asdfds").state("asdfd").country("asdfds")
				.build();
		ProfileDto profileDto = ProfileDto.builder().address(addressDto).age(23).caste("sjd")
				.email("manojmk2209@gmail.com").firstName("Manoj").lastName("kumar").gender(Gender.MALE).income(12345)
				.maritalStatus(MaritalStatus.SINGLE).profession("kjh").religion(Religion.HINDU).build();
		ApiResponse apiResponse = ApiResponse.builder().httpStatus(200l).build();
		Mockito.when(profileService.updateProfile("manojmk2209@gmail.com", profileDto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity = profileController.updateProfile("manojmk2209@gmail.com",
				profileDto);
		assertNotNull(responseEntity);
		assertEquals(200, responseEntity.getBody().getHttpStatus());

	}

	@Test
	void testProfileMatch() {
		AddressDto addressDto = AddressDto.builder().lane("asd").city("asdfds").state("asdfd").country("asdfds")
				.build();
		ProfileDto profileDto = ProfileDto.builder().address(addressDto).age(23).caste("sjd")
				.email("manojmk2209@gmail.com").firstName("Manoj").lastName("kumar").gender(Gender.MALE).income(12345)
				.maritalStatus(MaritalStatus.SINGLE).profession("kjh").religion(Religion.HINDU).build();
		List<ProfileDto> dtos = new ArrayList<>();
		dtos.add(profileDto);
		Mockito.when(profileService.getMatches("manojk@gmail.com", "male", 21, 29, "Single", "Hindu", "Lingayat"))
				.thenReturn(dtos);
		ResponseEntity<List<ProfileDto>> profileDtos = profileController.getMatches("manojk@gmail.com", "male", 21, 29,
				"Single", "Hindu", "Lingayat");
		assertEquals(1, profileDtos.getBody().size());
	}

	@Test
	void testShowInterest() {
		List<String> emails = new ArrayList<>();
		emails.add("manojmk2209@gmail.com");
		InterstedProfileDto interstedProfileDto = InterstedProfileDto.builder().emails(emails).build();
		ApiResponse apiResponse = ApiResponse.builder().httpStatus(201l).build();
		Mockito.when(profileService.showInterest("manojmk2209@gmail.com", interstedProfileDto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity = profileController.showInterest("manojmk2209@gmail.com",
				interstedProfileDto);
		assertNotNull(responseEntity);
		assertEquals(201, responseEntity.getBody().getHttpStatus());
	}
}
