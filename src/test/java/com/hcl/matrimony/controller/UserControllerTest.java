package com.hcl.matrimony.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.matrimony.dto.ApiResponse;
import com.hcl.matrimony.dto.RegisterDto;
import com.hcl.matrimony.entity.User;
import com.hcl.matrimony.service.UserService;

@ExtendWith(SpringExtension.class)
class UserControllerTest {
	@InjectMocks
	private UserController userController;
	@Mock
	private UserService userService;
	@Test
	void testregisterSuccess()
	{
		RegisterDto registerDto = RegisterDto.builder().email("megha@gmail.com").password("6gfh").userName("megha").build();
		ApiResponse apiResponse = ApiResponse.builder().httpStatus(201l).build();
		Mockito.when(userService.register(registerDto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity=userController.register(registerDto);
		assertEquals(201,responseEntity.getBody().getHttpStatus());
	}
	
	@Test
	void testLogin()
	{
		User user = User.builder().email("megha@gmail.com").password("Megha@123").build();
		ApiResponse apiResponse = ApiResponse.builder().httpStatus(200l).build();
		Mockito.when(userService.login(user.getEmail(),user.getPassword())).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity=userController.login(user.getEmail(),user.getPassword());
		assertEquals(200,responseEntity.getBody().getHttpStatus());
			
	}
	
	@Test
	void testLogout()
	{
		User user = User.builder().email("megha@gmail.com").build();
		ApiResponse apiResponse = ApiResponse.builder().httpStatus(200l).build();
		Mockito.when(userService.logout(user.getEmail())).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity =userController.logout(user.getEmail());
		assertEquals(200,responseEntity.getBody().getHttpStatus());
		
		
	}
 
}