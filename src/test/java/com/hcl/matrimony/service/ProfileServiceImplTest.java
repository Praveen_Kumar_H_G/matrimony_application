package com.hcl.matrimony.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.matrimony.dto.AddressDto;
import com.hcl.matrimony.dto.ApiResponse;
import com.hcl.matrimony.dto.InterstedProfileDto;
import com.hcl.matrimony.dto.ProfileDto;
import com.hcl.matrimony.entity.Address;
import com.hcl.matrimony.entity.Gender;
import com.hcl.matrimony.entity.MaritalStatus;
import com.hcl.matrimony.entity.Profile;
import com.hcl.matrimony.entity.Religion;
import com.hcl.matrimony.entity.User;
import com.hcl.matrimony.exception.ResourceConflictExists;
import com.hcl.matrimony.exception.ResourceNotFound;
import com.hcl.matrimony.exception.UnauthorizedUser;
import com.hcl.matrimony.repository.InterestedProfileRepository;
import com.hcl.matrimony.repository.ProfileRepository;
import com.hcl.matrimony.repository.UserRepository;
import com.hcl.matrimony.service.impl.ProfileServiceImpl;

@ExtendWith(SpringExtension.class)
class ProfileServiceImplTest {
	@Mock
	private UserRepository userRepository;
	@Mock
	private ProfileRepository profileRepository;
	@Mock
	private InterestedProfileRepository interestedProfileRepository;
	@InjectMocks
	private ProfileServiceImpl profileServiceImpl;

	@Test
	void testUpdateProfileSucess() {
		User user = User.builder().email("manojmk2209@gmail.com").password("adk2432").loggedIn(true).build();
		AddressDto addressDto = AddressDto.builder().lane("asd").city("asdfds").state("asdfd").country("asdfds")
				.build();
		ProfileDto profileDto = ProfileDto.builder().address(addressDto).age(23).caste("sjd")
				.email("manojmk2209@gmail.com").firstName("Manoj").lastName("kumar").gender(Gender.MALE).income(12345)
				.maritalStatus(MaritalStatus.SINGLE).profession("kjh").religion(Religion.HINDU).build();
		Profile profile = Profile.builder().age(profileDto.getAge()).build();
		Mockito.when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.of(user));
		Mockito.when(profileRepository.save(profile)).thenReturn(profile);
		ApiResponse apiResponse = profileServiceImpl.updateProfile(user.getEmail(), profileDto);
		assertEquals("Profile updated sucessfully", apiResponse.getMessage());
	}

	@Test
	void testUserNotFound() {
		AddressDto addressDto = AddressDto.builder().lane("asd").city("asdfds").state("asdfd").country("asdfds")
				.build();
		ProfileDto profileDto = ProfileDto.builder().address(addressDto).age(23).caste("sjd")
				.email("manojmk2209@gmail.com").firstName("Manoj").lastName("kumar").gender(Gender.MALE).income(12345)
				.maritalStatus(MaritalStatus.SINGLE).profession("kjh").religion(Religion.HINDU).build();
		Mockito.when(userRepository.findByEmail("kjsqws")).thenReturn(Optional.empty());
		assertThrows(ResourceNotFound.class, () -> profileServiceImpl.updateProfile("manojk", profileDto));

	}

	@Test
	void testUserNotLoggedIn() {
		User user = User.builder().email("manojmk2209@gmail.com").password("adk2432").loggedIn(false).build();
		AddressDto addressDto = AddressDto.builder().lane("asd").city("asdfds").state("asdfd").country("asdfds")
				.build();
		ProfileDto profileDto = ProfileDto.builder().address(addressDto).age(23).caste("sjd")
				.email("manojmk2209@gmail.com").firstName("Manoj").lastName("kumar").gender(Gender.MALE).income(12345)
				.maritalStatus(MaritalStatus.SINGLE).profession("kjh").religion(Religion.HINDU).build();
		Mockito.when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.of(user));
		assertThrows(UnauthorizedUser.class,
				() -> profileServiceImpl.updateProfile("manojmk2209@gmail.com", profileDto));

	}

	@Test
	void testUserMismatch() {
		User user = User.builder().email("manojmk2209@gmail.com").password("adk2432").loggedIn(true).build();
		AddressDto addressDto = AddressDto.builder().lane("asd").city("asdfds").state("asdfd").country("asdfds")
				.build();
		ProfileDto profileDto = ProfileDto.builder().address(addressDto).age(23).caste("sjd")
				.email("manojmk220@gmail.com").firstName("Manoj").lastName("kumar").gender(Gender.MALE).income(12345)
				.maritalStatus(MaritalStatus.SINGLE).profession("kjh").religion(Religion.HINDU).build();
		Mockito.when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.of(user));
		assertThrows(UnauthorizedUser.class,
				() -> profileServiceImpl.updateProfile("manojmk2209@gmail.com", profileDto));
	}

	@Test
	void testMatchFound() {
		User user = User.builder().email("manojmk2209@gmail.com").password("adk2432").loggedIn(true).build();
		Address address = Address.builder().lane("asd").city("asdfds").state("asdfd").country("asdfds").addressId(123)
				.build();
		Mockito.when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.of(user));
		Profile profile = Profile.builder().address(address).age(23).caste("asdfds").email("manojmk209@gmail.com")
				.firstName("sj").lastName("ksjd").gender(Gender.MALE.toString()).income(12345)
				.maritalStatus(MaritalStatus.SINGLE.toString()).profession("skja").religion(Religion.HINDU.toString())
				.build();
		List<Profile> profiles = new ArrayList<>();
		profiles.add(profile);
		Mockito.when(profileRepository
				.findByGenderIgnoreCaseAndAgeBetweenAndMaritalStatusIgnoreCaseAndReligionIgnoreCaseAndCasteIgnoreCase(
						profile.getGender(), 1, 24, profile.getMaritalStatus(), profile.getReligion(),
						profile.getCaste()))
				.thenReturn(profiles);
		Mockito.when(profileRepository.findByEmail(anyString())).thenReturn(null);
		List<ProfileDto> dtos = profileServiceImpl.getMatches(user.getEmail(), profile.getGender(), 1, 24,
				profile.getMaritalStatus(), profile.getReligion(), profile.getCaste());
		assertEquals(1, dtos.size());
	}

	@Test
	void testUserNotPresent() {
		Mockito.when(userRepository.findByEmail("manojk@gmail.com")).thenReturn(Optional.empty());
		assertThrows(ResourceNotFound.class,
				() -> profileServiceImpl.getMatches("manojk@gmail.com", "male", 2, 23, "Single", "Hindu", "lingayat"));
	}

	@Test
	void testUserNotLoggedInForMatch() {
		User user = User.builder().email("manojk@gmail.com").password("adk2432").loggedIn(false).build();
		Mockito.when(userRepository.findByEmail("manojk@gmail.com")).thenReturn(Optional.of(user));
		assertThrows(UnauthorizedUser.class,
				() -> profileServiceImpl.getMatches("manojk@gmail.com", "male", 2, 23, "Single", "Hindu", "lingayat"));
	}

	@Test
	void testShowInterestSuucess() {
		User user = User.builder().email("manojk@gmail.com").password("adk2432").loggedIn(true).build();
		List<String> emails=new ArrayList<>();
		emails.add("manojmk2209@gmail.com");
		InterstedProfileDto interstedProfileDto=InterstedProfileDto.builder().emails(emails).build();
		Mockito.when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.of(user));
		Mockito.when(interestedProfileRepository.saveAll(anyList())).thenReturn(anyList());
		ApiResponse apiResponse=profileServiceImpl.showInterest("manojk@gmail.com", interstedProfileDto);
		assertEquals("Profiles Added to interested", apiResponse.getMessage());
	}
	@Test
	void testUserNotPresentForInterest() {
		List<String> emails=new ArrayList<>();
		emails.add("manojmk2209@gmail.com");
		InterstedProfileDto interstedProfileDto=InterstedProfileDto.builder().emails(emails).build();
		Mockito.when(userRepository.findByEmail("manojk@gmail.com")).thenReturn(Optional.empty());
		assertThrows(ResourceNotFound.class,
				() -> profileServiceImpl.showInterest("manojk@gmail.com",interstedProfileDto ));
	}
	
	@Test
	void testUserNotLoggedInForInterest() {
		User user = User.builder().email("manojk@gmail.com").password("adk2432").loggedIn(false).build();
		List<String> emails=new ArrayList<>();
		emails.add("manojmk2209@gmail.com");
		InterstedProfileDto interstedProfileDto=InterstedProfileDto.builder().emails(emails).build();
		Mockito.when(userRepository.findByEmail("manojk@gmail.com")).thenReturn(Optional.of(user));
		assertThrows(UnauthorizedUser.class,
				() -> profileServiceImpl.showInterest("manojk@gmail.com",interstedProfileDto ));
	}
	
	@Test
	void testUserConflictsForInterest() {
		User user = User.builder().email("manojk@gmail.com").password("adk2432").loggedIn(true).build();
		List<String> emails=new ArrayList<>();
		emails.add("manojk@gmail.com");
		InterstedProfileDto interstedProfileDto=InterstedProfileDto.builder().emails(emails).build();
		Mockito.when(userRepository.findByEmail("manojk@gmail.com")).thenReturn(Optional.of(user));
		assertThrows(ResourceConflictExists.class,
				() -> profileServiceImpl.showInterest("manojk@gmail.com",interstedProfileDto ));
	}
}
