package com.hcl.matrimony.entity;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Profile {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long profileId;
	private String firstName;
	private String lastName;
	private String gender;
	private String religion;
	private String maritalStatus;
	private String caste;
	private String email;
	private int age;
	private double income;
	@OneToOne
	@Cascade(CascadeType.ALL)
	private Address address;
	private String zodiacSign;
	private String profession;
}
