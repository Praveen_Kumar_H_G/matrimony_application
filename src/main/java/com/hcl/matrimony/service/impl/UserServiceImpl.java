package com.hcl.matrimony.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.matrimony.dto.ApiResponse;
import com.hcl.matrimony.dto.RegisterDto;
import com.hcl.matrimony.entity.User;
import com.hcl.matrimony.exception.ResourceConflictExists;
import com.hcl.matrimony.exception.ResourceNotFound;
import com.hcl.matrimony.exception.UnauthorizedUser;
import com.hcl.matrimony.repository.UserRepository;
import com.hcl.matrimony.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	private final UserRepository userRepository;

	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;

	}

	@Override
	public ApiResponse register(RegisterDto registerDto) {
		Optional<User> user = userRepository.findByEmail(registerDto.getEmail());
		if (user.isPresent()) {
			throw new ResourceConflictExists("User already Registered");
		}
		User user2 = User.builder().email(registerDto.getEmail()).password(registerDto.getPassword())
				.userName(registerDto.getUserName()).build();
		userRepository.save(user2);
		return ApiResponse.builder().httpStatus(201l).message("User Registered sucessfully").build();
	}

	@Override
	public ApiResponse login(String email, String password) {
		User user = userRepository.findByEmail(email).orElseThrow(() -> new ResourceNotFound("User not found "));
		if (user.getPassword().equals(password)) {
			user.setLoggedIn(true);
			userRepository.save(user);
			return ApiResponse.builder().message("loggedin sucessfully ").httpStatus(200l).build();
		} else {
			throw new UnauthorizedUser("Unauthorized user");
		}
	}

	@Override
	public ApiResponse logout(String email) {
		User user = userRepository.findByEmail(email).orElseThrow(() -> new ResourceNotFound("User not exists"));
		user.setLoggedIn(false);
		userRepository.save(user);
		return ApiResponse.builder().message("logged out successfully").httpStatus(200l).build();
	}

}
