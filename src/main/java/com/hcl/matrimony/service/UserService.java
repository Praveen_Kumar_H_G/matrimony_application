package com.hcl.matrimony.service;

import com.hcl.matrimony.dto.ApiResponse;
import com.hcl.matrimony.dto.RegisterDto;

public interface UserService {

	ApiResponse register(RegisterDto registerDto);

	ApiResponse login(String email, String password);

	ApiResponse logout(String email);

}
