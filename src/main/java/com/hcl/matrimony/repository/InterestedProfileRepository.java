package com.hcl.matrimony.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.matrimony.entity.InterestedProfile;

public interface InterestedProfileRepository extends JpaRepository<InterestedProfile,Long>{

}
