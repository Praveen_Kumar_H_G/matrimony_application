package com.hcl.matrimony.dto;

import com.hcl.matrimony.entity.Gender;
import com.hcl.matrimony.entity.MaritalStatus;
import com.hcl.matrimony.entity.Religion;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProfileDto {
	@NotBlank(message = "firstName required")
	private String firstName;
	private String lastName;
	private Gender gender;
	private Religion religion;
	private MaritalStatus maritalStatus;
	@NotBlank(message = "caste required")
	private String caste;
	@Email(message = "invalid email")
	@NotBlank
	private String email;
	@Min(value = 18,message = "age must be greater than or equal to 18")
	private int age;
	private double income;
	private String zodiacSign;
	private String profession;
	@Valid
	private AddressDto address;
}
